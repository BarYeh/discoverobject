import sys
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

def deriveArray(array, d = 1):
    return [array[i] - array[i - d] for i in range(d, len(array))]

def maxIndex(numbers):
    maxNum = max(numbers)
    return [i for i, x in enumerate(numbers) if x == maxNum][0]

def parseLine(str):
    values = str.split(',')
    measurements = [int(value) for value in values[6:]]
    triplets = [measurements[i:i + 3] for i in range(0, len(measurements), 3)]
    return (int(values[0]), triplets)

path = './put-on.txt' if len(sys.argv) < 2 else sys.argv[1]
fileLines = ''
with open(path) as f:
    fileLines = f.readlines()
    # for line in fileLines:
    #     print(line[:-3])

data = [parseLine(line[:-3]) for line in fileLines]

def getTotalWeight(samples):
    return sum([sample[2] for sample in samples])

def getTotalWeights(samples):
    return [getTotalWeight(sample[1]) for sample in samples]

def getTimestampArray(samples):
    return [sample[0] for sample in samples]

def getFrame(frame, Ysize, Xsize):
    res = [[0 for i in range(Xsize)] for j in range(Ysize)]
    for sample in frame:
        res[sample[0]][sample[1]] = sample[2]
    return res

def getFrames(samples):
    Xs = [sample[1] for frame in samples for sample in frame[1]]
    Ys = [sample[0] for frame in samples for sample in frame[1]]
    maxX = max(Xs)
    maxY = max(Ys)
    return [getFrame(sample[1], maxY + 1, maxX + 1) for sample in samples]

totalWeights = getTotalWeights(data)
timestamps = getTimestampArray(data)
frames = getFrames(data)

def showHeatmap(frame):
    dataset=pd.DataFrame(data=frame)
    sns.heatmap(dataset,cmap="viridis")
    # plt.show()

def getChangeTimeIndex(show=True):
    d = 3
    weightDeltas = deriveArray(totalWeights, d)
    index = maxIndex(weightDeltas)
    if (show):
        plt.plot(timestamps, totalWeights)
        plt.annotate(
            'item moved',
            xy=(timestamps[index], max(totalWeights)),
            xytext=(timestamps[index] + 2000, max(totalWeights)*2/3),
            arrowprops=dict(
                facecolor='black',
                shrink=0.05
            )
        )

        plt.show()
    return index

def getBoundingRect(frame):
    allValues = [w for line in frame for w in line]
    maxValue = max(allValues)
    maxY = len(frame)
    maxX = len(frame[0])
    portion = 0.01
    buffer = portion * maxValue
    maxValueX = 0
    maxValueY = 0
    # find item x, y
    for y in range(maxY):
        lineMax = max(frame[y])
        if lineMax == maxValue:
            for x in range(maxX):
                if (frame[y][x] == maxValue):
                    maxValueX = x
                    break
            maxValueY = y
            break

    # find start y
    startY = 0
    for y in range(maxValueY, 0, -1):
        lineMax = max(frame[y])
        print('y ' + str(y))
        if lineMax < buffer:
            startY = y + 1
            break

    # find end y
    endY = maxY - 1
    for y in range(maxValueY, maxY):
        lineMax = max(frame[y])
        if lineMax < buffer:
            endY = y - 1
            break
    
    # find start x
    startX = 0
    for x in range(maxValueX, 0, -1):
        lineMax = max([frame[y][x] for y in range(maxY)])
        if lineMax < buffer:
            startX = x + 1
            break

    # find end x
    endX = maxX - 1
    for x in range(maxValueX, endX):
        lineMax = max([frame[y][x] for y in range(maxY)])
        if lineMax < buffer:
            endX = x - 1
            break
    
    print('startX ' + str(startX))
    print('endX ' + str(endX))
    print('startY ' + str(startY))
    print('endY ' + str(endY))
    return ((startY, startX), (endY, endX))


changeTime = getChangeTimeIndex()
boundingRect = getBoundingRect(frames[changeTime + 5])
width = boundingRect[1][1] - boundingRect[0][1]
height = boundingRect[1][0] - boundingRect[0][0]

import matplotlib.patches as patches

frame = frames[changeTime + 5] if totalWeights[0] < totalWeights[-1] else frames[changeTime - 5]
showHeatmap(frame)
plt.show()
fig1 = plt.figure()
ax1 = fig1.add_subplot(111, aspect='equal')
ax1.add_patch(
    patches.Rectangle((boundingRect[0][1], boundingRect[0][0]), width, height))
plt.ylim((0, len(frame)))
plt.xlim((0, len(frame[0])))
plt.show()
# for i in range(changeTime - 5, changeTime + 5):
#     print(i)
#     showHeatmap(frames[i])


# test if I can assume time is linear
# result: all positive, maximum 7.2% varience
# timestampDeltas = deriveArray(timestamps, 1);
# print('max change: ' + str(max(timestampDeltas)) + ' min: ' + str(min(timestampDeltas)))