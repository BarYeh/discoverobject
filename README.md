# DISCOVER OBJECT #

Test app to find time and location of an object placed on sensors
* Gets an input file path
* Shows a graph of total weight over time and marks the spot where the item is placed
* Shows a heatmap of the item on the sensors
* Shows the bounding box for the item (TODO - invert, place on heatmap, add fuzz)

### To run ###

* Clone the repo and cd into the directory
* run `python main.py`
* if a package is missing `pip install <package>` (matplotlib, seaborn)

# Notes #

Really enjoyed this task, I missed python and the intellectual challange of data processing was fun